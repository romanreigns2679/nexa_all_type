Docker Swarm -management of many nodes
    nodes = ec2s that belong to the swarm

docker swarm init --advertise-addr
    create a swarm and make the address available
        for many nodes

docker stack deploy -c path/docker-compose.yml name-of-stack
    start a stack of services using -c which means
        the configuration is from a docker-compose.yml