package com.example.demo.contoller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Planet;

@RestController
public class PlanetController {
	
	@RequestMapping("/")
	public List<Planet> getPlanets(){
		List<Planet> planetList = new ArrayList<Planet>();
		planetList.add(new Planet("Mercury", "Attachment issues"));
		planetList.add(new Planet("Venus", "tad hot"));
		planetList.add(new Planet("Earth", "1/5 really watery"));
		planetList.add(new Planet("Mars", "dusty"));
		planetList.add(new Planet("Jupiter", "too big"));
		planetList.add(new Planet("Saturn", "great at hula hoops"));
		planetList.add(new Planet("Uranus", "blueish"));
		planetList.add(new Planet("Neptune", "blue"));
		
		return planetList;
	}
	
}
