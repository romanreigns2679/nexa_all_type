package com.example.model;

public class Planet {
	
	private String name;
	private String description;

	public Planet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Planet(String name) {
		super();
		this.name = name;
	}
	
	public Planet(String name, String description) {
		this(name);
		this.description  = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Planet [name=" + name + ", description=" + description + "]";
	}
	
	
	

}
