### Docker

1.  What is containerization?
    
2.  What is the Docker Daemon?
    
3.  How are containers different from virtual machines? What makes docker containers more lightweight than virtual machines?
    
4.  What is a Docker image? Container?
    
5.  How is a Docker image different from a Docker container? How are the read/write layers different?
    
6.  List the steps to start Docker, create a Docker image, and spin up a container.
    
7.  What is the relevance of the Dockerfile to this process? List some keywords in the Dockerfile.
    
8.  What is the benefit to an image being built in Layers?
    
9.  What are some other Docker commands?
    
10.  What is a container registry? How would you retrieve and upload images to DockerHub?
    
11.  What is Docker compose and why is it useful?
    
12.  If you want to store state for a container, how does Docker recommend doing that?
    
13.  What is a Docker Network?