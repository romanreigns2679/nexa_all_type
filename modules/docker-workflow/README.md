## Docker workflow

This module covers:

## List of Topics
* [Dockerfile keywords](./dockerfile-keywords.md)
* [Building an image](./building-an-image.md)
* [Creating containers](./creating-containers.md)
* [Managing containers](./managing-containers.md)
* [Docker Command Cheatsheet](./docker-command-cheatsheet.md)
* [DockerHub container registry](./dockerhub-container-registry.md)
* [Docker compose](./docker-compose.md)
* [Docker Swarm](./docker-swarm.md)

## Prerequisites & Learning Objectives
No prereqs

After completing this module, associates should be able to:
* Use docker to containerize a project
* Build images using a Dockerfile
* Pull images from DockerHub
* Spin up containers from local or remote images
* View container logs and monitor container statuses
* Deploy containerized web applications that are bound to a host's port
* Stand up local services for testing using docker compose
