## Docker concepts

### Helpful Links

* [Docker](https://www.docker.com/resources)


This module covers:

## List of Topics
* [Containerization](./containerization.md)
* [Containers and virtual machines](./container-virtual-machine.md)
* [Installing docker](./installing-docker.md)
* [Docker architecture](./docker-architecture.md)
* [Docker daemon](./docker-daemon.md)
* [Docker images](./docker-images.md)
* [Docker containers](./docker-containers.md)
* [Dockerfile](./dockerfile-notes.md)
* [Persisting data with volumes](./docker-volumes.md)
* [Docker best practices](./docker-best-practices.md)

## Prerequisites & Learning Objectives
No prereqs

After completing this module, associates should be able to:
* Describe the benefits and use cases of containerization technology
* Explain the differences between containers and virtual machines
* Explain the difference between a Docker image and container
* List some Dockerfile keywords and explain what they do